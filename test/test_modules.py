import unittest
from ml4proflow import modules
from ml4proflow_mods.acoustic_ssad.modules import LearnedFeatures
from ml4proflow_mods.acoustic_ssad.modules import PipelineOneClassSVM

class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()

    def test_create_ommodule(self):
        lf = LearnedFeatures(self.dfm, {})
        self.assertIsInstance(lf, LearnedFeatures)
        pocsvm = PipelineOneClassSVM(self.dfm, {})
        self.assertIsInstance(pocsvm, LearnedFeatures)

if __name__ == '__main__':  # pragma: no cover
    unittest.main()
