from __future__ import annotations
from logging import raiseExceptions
from typing import Any

import os
import numpy as np
import pandas as pd
import tensorflow as tf
import librosa
from numpy.lib.stride_tricks import sliding_window_view as sliding_window
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.svm import OneClassSVM

from ml4proflow.modules import (Module, DataFlowManager, SourceModule)

class LearnedFeatures(Module):
    """Learned Features Module

    TODO

    Args:
        Module (_type_): _description_
    """
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)
        self.config = config
        self.target_samplerate = 16000

        if self.config["model_type"] == "samplecnn":
            self.loaded_model_name = "samplecnn_" + self.config["model_size"] + "_singlefeature"
            self.feature_size = 512
        elif self.config["model_type"] == "samplecnn+se":
            self.loaded_model_name = "samplecnn+se_" + self.config["model_size"] + "_multifeature"
            self.feature_size = 1024
            
        if self.config["model_size"] == "large":
            self.input_size = 59049
        elif self.config["model_size"] == "medium":
            self.input_size = 19683
        elif self.config["model_size"] == "small":
            self.input_size = 16384
        
        self.window_step_size = int(self.input_size / self.config["window_step_ratio"])      
        self.loaded_model = tf.keras.models.load_model(os.path.join(self.config["path2model"], self.loaded_model_name, 'models', 'model_checkpoint'))
        self.loaded_model.trainable = False
        if self.config["model_type"] == 'samplecnn+se':
            self.out = self.loaded_model.layers[-6].output
        elif self.config["model_type"] == 'samplecnn':
            self.out = self.loaded_model.layers[-3].output
        else:
            raise ValueError("model type not known")
        self.model = tf.keras.models.Model(inputs=[self.loaded_model.layers[0].input], outputs=[self.out])

    def resample_and_slide(self, x, original_samplerate, target_samplerate, width, stepsize):
        data = librosa.resample(
            y = x, 
            orig_sr=original_samplerate, 
            target_sr=target_samplerate
        )
        return sliding_window(data, width)[::stepsize, :]

    def transform(self, row: pd.DataFrame) -> pd.DataFrame:
        row[self.config["column2use_out_x"]] = self.model.predict(
            self.resample_and_slide(
                x = row[self.config["column2use_in_x"]], 
                original_samplerate = self.config["original_samplerate"],
                target_samplerate = self.target_samplerate,
                width = self.input_size,
                stepsize = self.window_step_size
            )
        )
        if ("column2use_in_y" in self.config) & ("column2use_out_y" in self.config) & ("y_accepted_classes" in self.config):
            if row[self.config["column2use_in_y"]] in self.config["y_accepted_classes"]:
                row[self.config["column2use_out_y"]] = np.ones(shape=[len(row[self.config["column2use_out_x"]]), ], dtype=np.int32)
            else:
                row[self.config["column2use_out_y"]] = (np.ones(shape=[len(row[self.config["column2use_out_x"]]), ], dtype=np.int32) * -1)
        return row

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        """Helper function:
        
        TODO

        On Event "New Data", this function ...

        :param name: Name of the channel
        :param sender: Module, providing the data
        :param data: Provided data frame
        :return: None
        """
        data = data.apply(lambda row: self.transform(row=row), axis=1)
        try:
            self._push_data(self.channel_push[0], data)
        except:
            return data

class PipelineOneClassSVM(Module):
    """ML-Pipeline for One-class SVM

    TODO

    Args:
        Module (_type_): _description_
    """
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)
        self.config = config
        # check training_type
        if self.config["training_type"] == "pca+oc_svm":
            self.clf_pipe = Pipeline(steps=[
                ('pca', PCA(
                    n_components = self.config["pca-n_components"], 
                    random_state = self.config["pca-random_state"])),
                ('oc_svm', OneClassSVM(
                    kernel = self.config["oc_svm-kernel"], 
                    gamma = self.config["oc_svm-gamma"], 
                    nu = self.config["oc_svm-nu"]))
            ])
        elif self.config["training_type"] == "oc_svm":
            self.clf_pipe = Pipeline(steps=[
                ('OC-SVM', OneClassSVM(
                    kernel = self.config["oc_svm-kernel"], 
                    gamma = self.config["oc_svm-gamma"], 
                    nu = self.config["oc_svm-nu"]))
            ])
        else:
            raise ValueError("training_type %s not known or not set." % (self.config["training_type"]))

    def fit(self, x_train:np.array, y_train:np.array):
        self.clf_pipe.fit(
            X=x_train, 
            y=y_train,
        )

    def predict(self, x_predict:np.array) -> np.array:
        return self.clf_pipe.predict(x_predict)

    def predict_on_df(self, row: pd.DataFrame) -> pd.DataFrame:
        # predict
        x_predict = self.predict(row[self.config["x_column"]])
        row[self.config["y_predict_column"]] = x_predict
        # acceptance
        no_of_rejected_windows = np.count_nonzero(x_predict == -1)
        if no_of_rejected_windows <= 2:
            row["acceptance"] = True
        else:
            row["acceptance"] = False
        return row

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        data = data.apply(lambda row: self.predict_on_df(row=row), axis=1)
        try:
            self._push_data(self.channel_push[0], data)
        except:
            return data