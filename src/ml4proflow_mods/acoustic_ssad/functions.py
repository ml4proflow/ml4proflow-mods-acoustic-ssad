import scipy
from scipy.signal import butter, lfilter, filtfilt
import numpy as np
import pandas as pd
import math
import librosa
import noisereduce as nr

def bp_butter(lowcut, highcut, fs, order=5):
    """bp_butter Butter Band Pass

    A good explanation can be found here: #https://stackoverflow.com/a/12233959

    Author: David Pelkmann (david.pelkmann@fh-bielefeld.de)

    :param lowcut: Lowcut frequency in Hz
    :type lowcut: float
    :param highcut: Highcut frequency in Hz
    :type highcut: float
    :param fs: Sampling frequenc of given input data
    :type fs: float
    :param order: Order of the filter, defaults to 5
    :type order: int, optional
    :return: Numerator (b) and denominator (a) polynomials of the IIR filter. Only returned if output='ba'.
    :rtype: array like, array like
    """
    # https://en.wikipedia.org/wiki/Nyquist_frequency
    nyq = 0.5 * fs 
    return butter(order, [(lowcut / nyq), (highcut / nyq)], btype='band', output='ba')

def bpf_butter(data, lowcut, highcut, fs, order=5, filtering="lfilter"):
    """bpf_butter Butter Band Pass Filter

    A good explanation can be found here: #https://stackoverflow.com/a/12233959

    Author: David Pelkmann (david.pelkmann@fh-bielefeld.de)

    :param data: Time series data which to be filtered
    :type data: array like
    :param lowcut: Lowcut frequency in Hz
    :type lowcut: float
    :param highcut: Highcut frequency in Hz
    :type highcut: float
    :param fs: Sampling frequenc of given input data
    :type fs: float
    :param order: Order of the filter, defaults to 5
    :type order: int, optional
    :param filtering: Type of filter. More informations: https://docs.scipy.org/doc/scipy/reference/signal.html#filtering
    :type string, optional
    :return: Filtered input data
    :rtype: array like
    """
    b, a = bp_butter(lowcut, highcut, fs, order=order)
    if filtering == "lfilter": # phase shift
        return lfilter(b, a, data)
    elif filtering == "filtfilt": # no phase shift
        return filtfilt(b, a, data)
                        
def seg_via_spl_border(signal:np.array, samplerate:int, border:float=0.75, lowcut:int=18000, highcut:int=22000, order:int=3, rms_frame_length:int=2048, rms_hop_length:int=512) -> np.array:
    """seg_via_border segmentation of sound signal via sound pressure level threshold

    Segmentation of the given input signal via the sound preassure level of bandpassed freq. area.

    Author: David Pelkmann (david.pelkmann@fh-bielefeld.de)

    Parameters
    ----------
    signal : np.array
        array of sound channels
    samplerate : int
        samplerate
    border : float, optional
        relative weighting to highest sound preassure as threshold, by default 0.75
    lowcut : int, optional
        lowcut frequency, by default 19000
    highcut : int, optional
        highcut frequency, by default 21000
    order : int, optional
        bandpass filter order, by default 3
    frame_length : int, optional
        frame to calculate rms for sound preassure level, by default math.pow(2, 13)

    Returns
    -------
    spl_border_segments_over_time : np.array
        2 dimensional array with shape (4, len(spl_signal)). [0] (float) spl time steps; [1] (float) spl signal; [2] (int) label above border; [3] (int) label number of segment. Transformation to a pandas dataframe: df_spl_segments = pd.DataFrame(spl_border_segments_over_time, columns=['time_steps', 'spl', 'spl_above_border', 'segment']) 
    """
    # initialize
    spl = list()
    
    # formular for spl: https://en.wikipedia.org/wiki/Sound_pressure#Sound_pressure_level
    # more than on channel (calculate spl mean of all channel)
    if signal.ndim == 2:
        signal_length = signal.shape[1]
        for channel in signal:
            f_signal = bpf_butter(data=channel, lowcut=lowcut, highcut=highcut, fs=samplerate, order=order)
            spl.append([20*math.log(p/2e-05,10) for p in librosa.feature.rms(y=f_signal, frame_length=int(rms_frame_length), hop_length=int(rms_hop_length)).T])
            #spl.append([20*math.log(p/2e-05,10) for p in librosa.feature.rms(y=f_signal, frame_length=int(rms_frame_length)).T])
        spl = np.mean(spl, axis=0)
    # one channel
    elif signal.ndim == 1:
        signal_length = signal.shape[0]
        f_signal = bpf_butter(data=signal, lowcut=lowcut, highcut=highcut, fs=samplerate, order=order)
        spl.append([20*math.log(p/2e-05,10) for p in librosa.feature.rms(y=f_signal, frame_length=int(rms_frame_length), hop_length=int(rms_hop_length)).T])
        spl = np.asarray(spl).T
    else:
        raise ValueError('signal dimension should be 1 or 2 but is %s' % (signal.ndim)) 
    spl_length = spl.shape[0]
    # array with time step per rms spl sample in spl_over_time
    spl_time_steps = np.asarray([x * (signal_length / samplerate) / spl_length for x in range(0, spl_length, 1)])

    if (border <= 1) & (border > 0):
        above_border = [True if x >= (max(spl) * border) else False for x in spl]
    else:
        above_border = [True if x >= (border) else False for x in spl]
    above_border[0] = False
    above_border[-1] = False
    segments, nobs = scipy.ndimage.label(above_border)
    
    spl_border_segments_over_time = np.c_[spl_time_steps, spl, above_border, segments]
    return spl_border_segments_over_time
    
def get_raw_segments(row:pd.DataFrame, col_name:str, sr:int, lowcut:int=None, highcut:int=None, channel:int=None, out_or_in:str=None, apply_spactral_gating:bool=True) -> pd.DataFrame:
    """get_raw_segments extract two raw driving segments

    Extracts for each channel the driving in and out phases of a given sound array

    Author: David Pelkmann (david.pelkmann@fh-bielefeld.de)

    Args:
        row (pd.DataFrame): dataframe row with the raw data array (has to be named: raw_data)
        col_name (str): column name to extract from
        sr (int): samplerate
        lowcut (int, optional): if not None an bandpass filter will be applied. Defaults to None.
        highcut (int, optional): if not None an bandpass filter will be applied. Defaults to None.
        channel (int, optional): channel to extract from. Defaults to None.
        out_or_in (str, optional): append driving out or in phase to the dataframe. Defaults to None.
        apply_spactral_gating (bool, optional): apply spectral gating with noisereduce or not. Defaults to True

    Raises:
        ValueError: if less than two segments are found (driving out and in are necessary)

    Returns:
        pd.DataFrame: processed dataframe
    """ 
    # spl_segments = ['time_steps', 'spl', 'spl_above_border', 'segment']
    spl_segments = seg_via_spl_border(row[col_name], sr)
    # less than expected two segments (in and out)
    if np.unique(spl_segments[:,2]).shape[0] < 2:
        raise ValueError('should be more than two segments found but only %s are found.' % (np.unique(spl_segments[:,2]).shape[0])) 
    nr_of_elements_per_seg = list()
    for segment in np.unique(spl_segments[:,3]):
        if segment == 0:
            continue
        else:
            segment_indeces = np.where(spl_segments[:,3] == segment)[0]
            # nr_of_elements_per_seg = [segment_nr, segment_length, indice_start, indice_end]
            nr_of_elements_per_seg.append([segment, segment_indeces[-1] - segment_indeces[0] + 1, segment_indeces[0], segment_indeces[-1]])
        biggest_segments = sorted(nr_of_elements_per_seg, key=lambda x: x[1], reverse=True)[0:2] # only two biggest segments
        biggest_segments.sort(key=lambda x: x[0])

    scale_factor = len(row['raw_data'][0]) / sr / len(spl_segments[:,1]) * sr
    
    try:
        in_start = int(biggest_segments[1][2] * scale_factor)
        in_end = int(biggest_segments[1][3] * scale_factor)
        out_start = int(biggest_segments[0][2] * scale_factor)
        out_end = int(biggest_segments[0][3] * scale_factor)
    except Exception as e:
        print(e)
        print(row)

    if channel == None:
        for i, channel in enumerate(row['raw_data']):
            if ((lowcut == None) & (highcut == None)):
                data = row['raw_data'][i]
            else:
                data = bpf_butter(data=row['raw_data'][i], lowcut=lowcut, highcut=highcut, fs=sr)
            if apply_spactral_gating:
                noise_seg_out = data[out_start:out_end]
                noise_seg_in = data[in_start:in_end]
                noise_clip = data[int(in_start * 0.935):int(in_start * 0.975)]
                if out_or_in == "out":
                    row[("c_%s_raw_out" % (i+1))] = nr.reduce_noise(y=noise_seg_out, sr=sr, y_noise=noise_clip, stationary=True)
                elif out_or_in == "in":
                    row[("c_%s_raw_in" % (i+1))] = nr.reduce_noise(y=noise_seg_in, sr=sr, y_noise=noise_clip, stationary=True)
                else:
                    row[("c_%s_raw_out" % (i+1))] = nr.reduce_noise(y=noise_seg_out, sr=sr, y_noise=noise_clip, stationary=True)
                    row[("c_%s_raw_in" % (i+1))] = nr.reduce_noise(y=noise_seg_in, sr=sr, y_noise=noise_clip, stationary=True)
            else:
                if out_or_in == "out":
                    row[("c_%s_raw_out" % (i+1))] = noise_seg_out
                elif out_or_in == "in":
                    row[("c_%s_raw_in" % (i+1))] = noise_seg_in
                else:
                    row[("c_%s_raw_out" % (i+1))] = noise_seg_out
                    row[("c_%s_raw_in" % (i+1))] = noise_seg_in
    else: 
        if ((lowcut == None) | (highcut == None)):
            data = row['raw_data'][channel-1]
        else:
            data = bpf_butter(data=row['raw_data'][channel-1], lowcut=lowcut, highcut=highcut, fs=sr)
        if apply_spactral_gating:
            noise_seg_out = data[out_start:out_end]
            noise_seg_in = data[in_start:in_end]
            noise_clip = data[int(in_start * 0.935):int(in_start * 0.975)]
            if out_or_in == "out":
                row[("c_%s_raw_out" % (channel))] = nr.reduce_noise(y=noise_seg_out, sr=sr, y_noise=noise_clip, stationary=True)
            elif out_or_in == "in":
                row[("c_%s_raw_in" % (channel))] = nr.reduce_noise(y=noise_seg_in, sr=sr, y_noise=noise_clip, stationary=True)
            else:
                row[("c_%s_raw_out" % (channel))] = nr.reduce_noise(y=noise_seg_out, sr=sr, y_noise=noise_clip, stationary=True)
                row[("c_%s_raw_in" % (channel))] = nr.reduce_noise(y=noise_seg_in, sr=sr, y_noise=noise_clip, stationary=True)
        else:
            if out_or_in == "out":
                row[("c_%s_raw_out" % (channel))] = noise_seg_out
            elif out_or_in == "in":
                row[("c_%s_raw_in" % (channel))] = noise_seg_in
            else:
                row[("c_%s_raw_out" % (channel))] = noise_seg_out
                row[("c_%s_raw_in" % (channel))] = noise_seg_in
    return row