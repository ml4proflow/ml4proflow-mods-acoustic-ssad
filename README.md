# ml4proflow-mods-acoustic-ssad

Acoustic Semi-supervised Anomaly Detection Module for ml4proflow

TODO

**ml4proflow** is an open source, platform independent framework for data processing in industrial applications. More information are available in the [main repository]().
  

# File structure

Overview of the file structure of a module:

```
ml4proflow-mods-module_name
├── setup.py                            #Installation script for pip
├── license.txt                         #License header
├── README.md                           #Readme for this module
├── src/ml4proflow_mods/module_name     #Source code   
|    ├── init.py
|    ├── modules.py
|    ├── widgets.py                     #Optional
└── test                                #Test scripts
|    ├── init.py
|    ├── test_modules.py
|    ├── test_widgets.py
└── docs                                #Documentation
|    ├── README.md
└── notebooks                           #Jupyter notebooks, optional
└── nodered                             #Node-red flows, optional
└── data                                #Example data, optional


```

  
# Usage
### Clone this repository 
```console 
$ git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template -ml4proflow-mods-'ModuleName'
```
### Configure the template
- [setup.py](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template/-/blob/master/setup.py): 
    - Lines with ``` #todo``` should be changed, other lines can remain the same
    - For other lines, see [here](https://setuptools.pypa.io/en/latest/userguide/index.html)
- [LICENSE.txt](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template/-/blob/master/LICENSE.txt)
- [modules.py](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template/-/blob/master/src/ml4proflow_mods/module_name/modules.py)
    - Source Code of your module
    - Rename folder ```./module_name```
- [test_modules.py](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template/-/blob/master/test/test_modules.py)
    - Source code of tests 

- Document the source code 
- Check code quality with mypy and flake8 

- Update the [README](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-template/-/blob/master/docs/README.md)

### Install your module
With the provided installation script ```setup.py```, you can easily install your module with ```conda``` or ```pip```.

To install your module in editable mode, run in your folder
``` $ pip install -e .``` 


### Documentation

Follow the steps of README in ```./docs```
