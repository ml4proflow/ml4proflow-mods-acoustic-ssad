from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "ml4proflow-mods-acoustic_ssad"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="David Pelkmann",
    author_email="david.pelkmann@fh-bielefeld.de",
    description="ml4proflow-Module to perform acoustiv semi-supervised anomaly detection",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: ",
        "Operating System :: OS Independent",
    ],
    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],
    package_dir={"": "src"},
    data_files=[('ml4proflow/notebooks', ['notebooks/usage_hanning.ipynb'])],
    # package_data={"ml4proflow": ["py.typed"], "OMPython": ["py.typed"]},
    entry_points={
        # 'console_scripts': [
        # 'ml4proflow-cli=ml4proflow.ml4proflow_cli:main',
        # ],
    },
    cmdclass=cmdclass,
    python_requires=">=3.7",  # tested only for 3.7.*
    install_requires=[
        "ml4proflow",
        "ipykernel",
        "numpy", # tested only for ==1.21.*
        "pandas",
        "h5py",
        "matplotlib",
        "scipy",
        "sklearn",
        "librosa",
        "noisereduce",
        "tensorflow", #tested only for ==2.4.*",
        "pickle5"
        ],
    extras_require={
        "tests": ["pytest"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
